//
//  Guesser.swift
//  Razeena_Guesser_app
//
//  Created by Student on 2/27/19.
//  Copyright © 2019 Student. All rights reserved.
//

import Foundation
struct Guess{
    var correctAnswer: Int
    var numAttemptsRequired: Int
    init(correctAnswer: Int, numAttemptsRequired: Int) {
        self.correctAnswer = correctAnswer
        self.numAttemptsRequired = numAttemptsRequired
    }
}
enum Result:String{
    case tooLow = "Too Low"
    case tooHigh = "Too High"
    case correct = "Correct"
}
class Guesser{
    private static var _shared:Guesser!
    static var shared: Guesser{
        if _shared == nil{
            _shared = Guesser()
        }
        return _shared
    }
    
    
    private var correctAnswer:Int
    private var _numAttempts:Int
    var numAttempts:Int{
        return _numAttempts
    }
    var guesses:[Guess]
    private init(correctAnswer: Int, numAttempts:Int, guesses: [Guess]) {
        self.correctAnswer = correctAnswer
        self._numAttempts = numAttempts
        self.guesses = guesses
    }
    private convenience init(){
        self.init(correctAnswer: Int.random(in: 1...10), numAttempts: 1, guesses:[])
    }
    
    func createNewProblem()  {
        correctAnswer = Int.random(in: 1...10)
//        correctAnswer = Int(arc4random_uniform(10)) + 1
        _numAttempts = 1
        
    }
    
    func amIRight(guess:Int) -> Result{
        if guess == correctAnswer{
            let guessInstance = Guess(correctAnswer: self.correctAnswer, numAttemptsRequired: numAttempts)
            guesses.append(guessInstance)
            return Result.correct
        }
        else if guess < correctAnswer{
            _numAttempts = _numAttempts + 1
            return Result.tooLow
        }
        else{
            _numAttempts = _numAttempts + 1
            return Result.tooHigh
        }
    }
    func guess(_ index:Int) -> Guess{
        return guesses[index]
    }
    func numGuesses() -> Int{
        print(guesses.count)
        return guesses.count
    }
    func clearStatistics(){
        guesses.removeAll()
    }
    func minimumNumAttempts() -> Int{
        var min:Int
        if guesses.count == 0 {
            min = 0
        } else {
            min = guesses[0].numAttemptsRequired
        }
        for data in guesses {
            if min >= data.numAttemptsRequired {
                min = data.numAttemptsRequired
            }
        }
        return min
    }
    func maximumNumAttempts() -> Int{
        var max:Int
        if guesses.count == 0 {
            max = 0
        } else {
            max = guesses[0].numAttemptsRequired
        }
        for data in guesses {
            if max <= data.numAttemptsRequired {
                max = data.numAttemptsRequired
            }
        }
        return max
    }
    func meanInAttempts() -> Double {
        var mean = 0.0
        for data in guesses {
            mean += Double(data.numAttemptsRequired)
        }
        if guesses.count != 0 {
            mean = mean / Double(guesses.count)
        }
        return mean
    }
    func stdDevInAttempts() -> Double {
        var stdDev = 0.0
        for data in guesses {
            stdDev += pow((Double(data.numAttemptsRequired) - meanInAttempts()),2.0)
        }
        if guesses.count != 0 {
            stdDev = sqrt(stdDev / Double(guesses.count))
        }
        return stdDev
    }
}
