//
//  FirstViewController.swift
//  Razeena_Guesser_app
//
//  Created by Student on 2/27/19.
//  Copyright © 2019 Student. All rights reserved.
//

import UIKit

class GuesserViewController: UIViewController {
    
    
    @IBOutlet weak var feedback: UILabel!
    func displayMessage(){
        let alert = UIAlertController(title: "Well done",
                                      message: "You got it in \(Guesser.shared.numAttempts) tries",
            preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default,
                                      handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBOutlet weak var my_guess: UITextField!
    
    
    @IBAction func am_i_right(_ sender: UIButton) {
        if let myGuess = Int(my_guess.text!) {
            if myGuess > 0 && myGuess < 11{
                feedback.text = Guesser.shared.amIRight(guess: myGuess).rawValue
                if feedback.text!.isEqual("Correct") {
                    displayMessage()
                }
            }
            else{
                let alertController = UIAlertController(title: "", message: "Enter an valid integer between 1 to 10", preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: .default, handler: nil)
                alertController.addAction(action)
                
                self.show(alertController, sender: nil)
            }
        }
        else{
            let alertController = UIAlertController(title: "", message: "Enter an integer", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(action)
            
            self.show(alertController, sender: nil)
        }
    }
    
    @IBAction func create_new_problem(_ sender: UIButton) {
        
        Guesser.shared.createNewProblem()
    }
}

