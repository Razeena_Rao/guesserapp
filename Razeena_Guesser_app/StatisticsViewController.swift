//
//  SecondViewController.swift
//  Razeena_Guesser_app
//
//  Created by Student on 2/27/19.
//  Copyright © 2019 Student. All rights reserved.
//

import UIKit

class StatisticsViewController: UIViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        minimum.text = "\(Guesser.shared.minimumNumAttempts())"
        maximum.text = "\(Guesser.shared.maximumNumAttempts())"
        mean.text = "\(Guesser.shared.meanInAttempts())"
        stdDev.text = "\(Guesser.shared.stdDevInAttempts())"
    }

    @IBOutlet weak var minimum: UILabel!
    
    @IBOutlet weak var maximum: UILabel!
    
    @IBOutlet weak var mean: UILabel!
    
    @IBOutlet weak var stdDev: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func clear(_ sender: UIButton) {
        minimum.text = "0"
        maximum.text = "0"
        mean.text = "0"
        stdDev.text = "0"
        Guesser.shared.clearStatistics()
    }
    
}

